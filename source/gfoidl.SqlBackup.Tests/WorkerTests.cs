﻿using System.Collections.Generic;
using NUnit.Framework;

namespace gfoidl.SqlBackup.Tests
{
    namespace WorkerTests
    {
        [TestFixture]
        public class EnsureBracketsAroundDatabase
        {
            [Test]
            [TestCaseSource("TestCases")]
            public string Database_is_given___Returns_name_with_brackets(string database)
            {
                string actual = Worker.EnsureBracketsAroundDatabase(database);

                return actual;
            }
            //---------------------------------------------------------------------
            private static IEnumerable<TestCaseData> TestCases()
            {
                yield return new TestCaseData("foo").Returns("[foo]");
                yield return new TestCaseData("foo.bar").Returns("[foo.bar]");

                yield return new TestCaseData("[foo]").Returns("[foo]");
                yield return new TestCaseData("[foo.bar]").Returns("[foo.bar]");
            }
        }
    }
}