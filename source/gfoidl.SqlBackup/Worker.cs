﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace gfoidl.SqlBackup
{
    internal class Worker
    {
        private readonly IConfiguration _config;
        private readonly IEmailNotifier _emailNotifier;
        //---------------------------------------------------------------------
        public Worker(IConfiguration config, IEmailNotifier emailNotifier)
        {
            _config        = config;
            _emailNotifier = emailNotifier;
        }
        //---------------------------------------------------------------------
        public string Run(string database, string mode)
        {
            string sql         = this.ConstructSql(database, mode);
            SqlConnection conn = new SqlConnection(_config.Connection);
            SqlCommand cmd     = new SqlCommand(sql, conn);

            Directory.CreateDirectory(_config.BaseBackupDirectory);

            try
            {
                conn.Open();
                cmd.CommandTimeout = 3600;  // Sekunden
                cmd.ExecuteNonQuery();

                return null;
            }
            catch (Exception ex)
            {
                if (_config.EmailOnError && _emailNotifier != null)
                    _emailNotifier.SendErrorEmail(database, ex);

                return ex.Message;
            }
            finally
            {
                cmd.Dispose();
                conn.Dispose();
            }
        }
        //---------------------------------------------------------------------
        private string ConstructSql(string database, string mode)
        {
            string sql        = this.GetScript(mode);
            string backupFile = this.GetBackupFileName(database);
            string timeStamp  = this.GetTimeStamp();

            database = EnsureBracketsAroundDatabase(database);

            sql = string.Format(sql, database, backupFile, timeStamp);

            return sql;
        }
        //---------------------------------------------------------------------
        private string GetScript(string mode)
        {
            string scriptName = string.Format("scripts/Backup_{0}.sql", mode);
            scriptName        = Path.Combine(Path.GetDirectoryName(typeof(Worker).Assembly.Location), scriptName);

            return File.ReadAllText(scriptName);
        }
        //---------------------------------------------------------------------
        private string GetBackupFileName(string database)
        {
            return GetBackupFileName(DateTime.Now, database, _config.BaseBackupDirectory);
        }
        //---------------------------------------------------------------------
        internal static string GetBackupFileName(DateTime now, string database, string baseBackupDirectory)
        {
            string fileName = string.Format(
                "{0}_Backup_{1}_KW{2:00}.bak",
                database,
                now.Year,
                CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(now, CalendarWeekRule.FirstDay, DayOfWeek.Monday));

            fileName = Path.Combine(baseBackupDirectory, fileName);

            return fileName;
        }
        //---------------------------------------------------------------------
        internal static string EnsureBracketsAroundDatabase(string database)
        {
            if (!Regex.IsMatch(database, @"\[[^\]]*\]"))
                database = "[" + database + "]";
            
            return database;
        }
        //---------------------------------------------------------------------
        private string GetTimeStamp()
        {
            return GetTimeStamp(DateTime.Now);
        }
        //---------------------------------------------------------------------
        internal static string GetTimeStamp(DateTime now)
        {
            return string.Format(
                "{0}-{1:00}-{2:00}_{3:00}_{4:00}",
                now.Year, now.Month, now.Day,
                now.Hour, now.Minute);
        }
    }
}