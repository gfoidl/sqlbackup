﻿namespace gfoidl.SqlBackup
{
    internal interface IConfiguration
    {
        string BaseBackupDirectory { get; }
        bool EmailOnError          { get; }
        string EmailAddress        { get; }
        string Connection          { get; }
    }
}