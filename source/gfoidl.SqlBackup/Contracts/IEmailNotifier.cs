﻿using System;

namespace gfoidl.SqlBackup
{
    internal interface IEmailNotifier
    {
        void SendErrorEmail(string database, Exception ex);
    }
}