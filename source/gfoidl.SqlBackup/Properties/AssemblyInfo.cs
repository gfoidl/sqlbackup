﻿using System.Reflection;
using gfoidl.SqlBackup;

[assembly: AssemblyTitle("gfoidl.SqlBackup")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Foidl Günther")]
[assembly: AssemblyProduct("gfoidl.SqlBackup")]
[assembly: AssemblyCopyright("Copyright © Foidl Günther 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(Program.Version)]
[assembly: AssemblyFileVersion(Program.Version)]