﻿using System;
using System.Diagnostics;

namespace gfoidl.SqlBackup
{
    class Program
    {
        internal const string Version = "1.1.2";
        //---------------------------------------------------------------------
        static void Main(string[] args)
        {
            string database = null;
            string mode     = null;

            if (args != null && args.Length == 2)
            {
                database = args[0];
                mode     = args[1];
            }
            else
            {
                Console.WriteLine("SqlBackup v{0} (c) 2016 Günther Foidl\n", Version);

                Console.Write("Database                  -> ");
                database = Console.ReadLine();

                Console.Write("Mode (full | diff | tran) -> ");
                mode = Console.ReadLine();
            }

            if (mode != "full" && mode != "diff" && mode != "tran")
                Console.WriteLine("unknown mode -- exiting");

            IConfiguration config       = Properties.Settings.Default;
            EmailNotifier emailNotifier = new EmailNotifier(config);

            try
            {
                DateTime start = DateTime.Now;
                Console.WriteLine("taking backup...");

                Worker worker = new Worker(config, emailNotifier);
                string msg    = worker.Run(database, mode);

                DateTime end = DateTime.Now;
                Console.WriteLine("backup of '{0}' ({1}) done in {2} seconds\n{3}", database, mode, (end - start).TotalSeconds, msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                if (config.EmailOnError)
                    emailNotifier.SendErrorEmail(string.Empty, ex);

                // So steht der Fehler im EventLog -- sonst würde er verschluckt
                throw;
            }

            Console.WriteLine("\nEnd.");
            if (Debugger.IsAttached)
                Console.ReadKey();
        }
    }
}