﻿using System;
using System.Net.Mail;

namespace gfoidl.SqlBackup
{
    internal class EmailNotifier : IEmailNotifier
    {
        private readonly IConfiguration _config;
        //---------------------------------------------------------------------
        public EmailNotifier(IConfiguration config)
        {
            _config = config;
        }
        //---------------------------------------------------------------------
        #region IEmailNotifier Members
        public void SendErrorEmail(string database, Exception ex)
        {
            using (MailMessage email = new MailMessage())
            {
                foreach (string to in _config.EmailAddress.Split(';'))
                    email.To.Add(to);

                email.Subject = string.Format("SqlBackup for {0} -- Error", database);
                email.Body    = ex.Message;

                using (SmtpClient smtp = new SmtpClient())
                    smtp.Send(email);
            }
        }
        #endregion
    }
}