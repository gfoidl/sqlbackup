[![Build status](https://ci.appveyor.com/api/projects/status/8ipaf325ibyxdmhk?svg=true)](https://ci.appveyor.com/project/GntherFoidl/sqlbackup)

# Zweck

* Durchf�hrung von Backup der SQL Datenbanken  
* Email-Benachrichtigung falls das Backup nicht fehlerfrei verlief

## Umsetzung

Tool, das per Kommandozeile via Windows-Aufgabenplanung getriggert wird.

In der Befehlzeile kann angegeben werden:

* welche Datenbank  
* Full, Diff od. Tran  

Der Rest ist in der _app.config_.